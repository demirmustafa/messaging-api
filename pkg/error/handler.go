package error

import (
	"github.com/labstack/echo/v4"
)

func Handler(err error, c echo.Context) {
	if err != nil {
		c.Logger().Error(err)
		response := create(err)
		_ = c.JSON(response.Status, response)
	}
}
