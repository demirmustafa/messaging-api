package error

import (
	"encoding/json"
	"errors"

	"github.com/labstack/echo/v4"
)

type GErr struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

func (er GErr) Error() string {
	msg, _ := json.Marshal(er)
	return string(msg)
}

func create(err error) GErr {
	var errorResponse GErr
	if errors.As(err, &errorResponse) {
		return errorResponse
	}
	var httpError *echo.HTTPError
	if errors.As(err, &httpError) {
		return GErr{
			Status:  httpError.Code,
			Message: httpError.Message.(string),
		}
	}

	return GErr{
		Status:  500,
		Message: "Internal Server Error",
	}
}
