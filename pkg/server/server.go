package server

import (
	"net/http"
	"time"

	errs "com.gitlab/demirmustafa/messaging-api/pkg/error"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/tylerb/graceful"
)

type Handler interface {
	RegisterRoutes(app *echo.Echo)
}

type Server struct {
	app  *echo.Echo
	port string
}

func New(port string, handlers []Handler) Server {
	app := echo.New()
	server := Server{
		app:  app,
		port: port,
	}

	server.app.Use(middleware.Recover())
	server.app.Use(middleware.Logger())

	server.app.HTTPErrorHandler = errs.Handler

	for _, handler := range handlers {
		handler.RegisterRoutes(server.app)
	}
	server.addRoutes()

	return server
}

func (s Server) Run() {
	s.app.Server.Addr = s.port
	err := graceful.ListenAndServe(s.app.Server, 10*time.Second)
	if err != nil {
		panic(err)
	}
}

func (s Server) addRoutes() {
	s.app.GET("/health", healthCheck())
}

func healthCheck() func(context echo.Context) error {
	return func(context echo.Context) error {
		return context.NoContent(http.StatusOK)
	}
}
