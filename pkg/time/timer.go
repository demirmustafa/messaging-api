package time

import "time"

type Timer interface {
	GetNow() *time.Time
}

type timer struct {
}

func NewTimer() Timer {
	return &timer{}
}

func (t *timer) GetNow() *time.Time {
	tm := time.Now()

	return &tm
}
