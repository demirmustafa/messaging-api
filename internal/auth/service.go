package auth

import (
	"context"
	"net/http"

	"com.gitlab/demirmustafa/messaging-api/internal/auth/model"
	"com.gitlab/demirmustafa/messaging-api/internal/user"
	error2 "com.gitlab/demirmustafa/messaging-api/pkg/error"
	"github.com/lestrrat-go/jwx/jwk"
)

type Service struct {
	userService user.UserService
	issuer      issuer
}

type issuer interface {
	Generate(payload map[string]interface{}) (string, error)
	PublicKeyJWKs() jwk.Key
	Parse(token string) (map[string]interface{}, error)
	IsTokenExpired(claims map[string]interface{}) bool
}

func NewService(userService user.UserService, issuer issuer) *Service {
	return &Service{
		userService: userService,
		issuer:      issuer,
	}
}

func (s Service) DoLogin(ctx context.Context, request *model.LoginRequest) (*model.LoginResponse, error) {
	unAuthErr := error2.GErr{
		Status:  http.StatusUnauthorized,
		Message: "unauthorized",
	}

	if err := validateRequest(request); err != nil {
		return nil, err
	}

	u, err := s.userService.GetUser(ctx, request.Username)
	if err != nil || u == nil {
		return nil, unAuthErr
	}

	if u.Password != request.Password {
		return nil, unAuthErr
	}

	token, err := s.issuer.Generate(map[string]interface{}{
		"username": u.Username, // claims
	})

	if err != nil {
		return nil, unAuthErr
	}

	return &model.LoginResponse{AccessToken: token}, nil
}

func validateRequest(req *model.LoginRequest) error {
	if req == nil || req.Username == "" || req.Password == "" {
		return error2.GErr{
			Status:  http.StatusUnauthorized,
			Message: "unauthorized",
		}
	}
	return nil
}
