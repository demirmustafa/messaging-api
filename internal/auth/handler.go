package auth

import (
	"context"
	"net/http"

	"com.gitlab/demirmustafa/messaging-api/internal/auth/model"
	"github.com/labstack/echo/v4"
)

type Handler struct {
	service service
}

type service interface {
	DoLogin(ctx context.Context, request *model.LoginRequest) (*model.LoginResponse, error)
}

func New(s service) *Handler {
	return &Handler{service: s}
}

func (h Handler) RegisterRoutes(app *echo.Echo) {
	app.POST("/login", h.Login)
}

func (h Handler) Login(c echo.Context) error {
	ctx := c.Request().Context()
	request := &model.LoginRequest{}
	err := c.Bind(request)
	if err != nil {
		return err
	}

	response, err := h.service.DoLogin(ctx, request)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, response)
}
