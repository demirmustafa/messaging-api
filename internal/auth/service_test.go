package auth

import (
	"context"
	"errors"
	"net/http"
	"testing"

	user_model "com.gitlab/demirmustafa/messaging-api/internal/user/model"
	"go.mongodb.org/mongo-driver/bson/primitive"

	auth_mocks "com.gitlab/demirmustafa/messaging-api/internal/auth/mocks"
	"com.gitlab/demirmustafa/messaging-api/internal/auth/model"
	user_mocks "com.gitlab/demirmustafa/messaging-api/internal/user/mocks"
	error2 "com.gitlab/demirmustafa/messaging-api/pkg/error"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestServiceShouldReturnUnAuthorizedErrorWhenRequestIsInvalid(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	userService := user_mocks.NewMockUserService(mockController)
	issuer := auth_mocks.NewMockissuer(mockController)

	service := NewService(userService, issuer)

	userService.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Times(0)
	issuer.EXPECT().Generate(gomock.Any()).Times(0)
	issuer.EXPECT().PublicKeyJWKs().Times(0)

	type testCase struct {
		request          *model.LoginRequest
		expectedResponse *model.LoginResponse
		expectedError    error
	}

	testCases := []testCase{
		{
			request: &model.LoginRequest{
				Username: "",
				Password: "testPassword123",
			},
			expectedResponse: nil,
			expectedError: error2.GErr{
				Status:  http.StatusUnauthorized,
				Message: "unauthorized",
			},
		},
		{
			request: &model.LoginRequest{
				Username: "testUser",
				Password: "",
			},
			expectedResponse: nil,
			expectedError: error2.GErr{
				Status:  http.StatusUnauthorized,
				Message: "unauthorized",
			},
		},
		{
			request:          nil,
			expectedResponse: nil,
			expectedError: error2.GErr{
				Status:  http.StatusUnauthorized,
				Message: "unauthorized",
			},
		},
	}

	for _, tc := range testCases {
		response, err := service.DoLogin(context.Background(), tc.request)
		assert.Equal(t, tc.expectedError, err)
		assert.Equal(t, tc.expectedResponse, response)
	}
}

func TestServiceShouldReturnUnAuthorizedErrorWhenUserNotRegistered(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	userService := user_mocks.NewMockUserService(mockController)
	issuer := auth_mocks.NewMockissuer(mockController)

	service := NewService(userService, issuer)

	userService.EXPECT().
		GetUser(gomock.Any(), gomock.Eq("testUser")).
		Return(nil, errors.New("user not found")).
		Times(1)

	issuer.EXPECT().Generate(gomock.Any()).Times(0)

	response, err := service.DoLogin(context.Background(), &model.LoginRequest{
		Username: "testUser",
		Password: "testPassword123",
	})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.Equal(t, error2.GErr{
		Status:  http.StatusUnauthorized,
		Message: "unauthorized",
	}, err)
}

func TestServiceShouldReturnUnAuthorizedErrorWhenPasswordsNotMatch(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	userService := user_mocks.NewMockUserService(mockController)
	issuer := auth_mocks.NewMockissuer(mockController)

	service := NewService(userService, issuer)

	userService.EXPECT().
		GetUser(gomock.Any(), gomock.Eq("testUser")).
		Return(&user_model.User{
			Email:    "user@test.com",
			Username: "testUser",
			Password: "notMatchingPassword",
		}, nil).
		Times(1)

	issuer.EXPECT().Generate(gomock.Any()).Times(0)

	response, err := service.DoLogin(context.Background(), &model.LoginRequest{
		Username: "testUser",
		Password: "testPassword123",
	})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.Equal(t, error2.GErr{
		Status:  http.StatusUnauthorized,
		Message: "unauthorized",
	}, err)
}

func TestServiceShouldReturnAccessTokenResponse(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	userService := user_mocks.NewMockUserService(mockController)
	issuer := auth_mocks.NewMockissuer(mockController)

	service := NewService(userService, issuer)

	docID := primitive.NewObjectID()
	userService.EXPECT().
		GetUser(gomock.Any(), gomock.Eq("testUser")).
		Return(&user_model.User{
			ID:       docID,
			Email:    "user@test.com",
			Username: "testUser",
			Password: "testPassword123",
		}, nil).
		Times(1)

	issuer.EXPECT().
		Generate(map[string]interface{}{
			"username": "testUser",
		}).
		Return("test token", nil).
		Times(1)

	response, err := service.DoLogin(context.Background(), &model.LoginRequest{
		Username: "testUser",
		Password: "testPassword123",
	})

	assert.Nil(t, err)
	assert.NotNil(t, response)
	assert.Equal(t, &model.LoginResponse{AccessToken: "test token"}, response)
}

func TestServiceShouldReturnUnAuthorizedErrorWhenErrorOccursWhileCreatingJWT(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	userService := user_mocks.NewMockUserService(mockController)
	issuer := auth_mocks.NewMockissuer(mockController)

	service := NewService(userService, issuer)

	docID := primitive.NewObjectID()
	userService.EXPECT().
		GetUser(gomock.Any(), gomock.Eq("testUser")).
		Return(&user_model.User{
			ID:       docID,
			Email:    "user@test.com",
			Username: "testUser",
			Password: "testPassword123",
		}, nil).
		Times(1)

	issuer.EXPECT().
		Generate(map[string]interface{}{
			"username": "testUser",
		}).
		Return("", errors.New("something went wrong")).
		Times(1)

	response, err := service.DoLogin(context.Background(), &model.LoginRequest{
		Username: "testUser",
		Password: "testPassword123",
	})

	assert.NotNil(t, err)
	assert.Nil(t, response)
	assert.Equal(t, error2.GErr{
		Status:  http.StatusUnauthorized,
		Message: "unauthorized",
	}, err)
}
