package auth

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	auth_mocks "com.gitlab/demirmustafa/messaging-api/internal/auth/mocks"
	"com.gitlab/demirmustafa/messaging-api/internal/auth/model"
	error2 "com.gitlab/demirmustafa/messaging-api/pkg/error"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestHandlerShouldLoginUser(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, recorder := func(request model.LoginRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(request)
		req := httptest.NewRequest(http.MethodPost, "/login", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		c := echo.New().NewContext(req, rec)

		return c, rec
	}(model.LoginRequest{
		Username: "testUser",
		Password: "testPassword123",
	})

	service := auth_mocks.NewMockservice(mockController)
	handler := New(service)

	service.EXPECT().
		DoLogin(gomock.Any(), gomock.Eq(&model.LoginRequest{
			Username: "testUser",
			Password: "testPassword123",
		})).
		Return(&model.LoginResponse{AccessToken: "test token"}, nil).
		Times(1)

	err := handler.Login(context)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, recorder.Code)

	response := &model.LoginResponse{AccessToken: "test token"}
	err = json.Unmarshal(recorder.Body.Bytes(), response)
	assert.Nil(t, err)
	assert.Equal(t, &model.LoginResponse{AccessToken: "test token"}, response)
}

func TestHandlerShouldReturnUnAuthorizedWhenUsernameNotProvided(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, _ := func(request model.LoginRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(request)
		req := httptest.NewRequest(http.MethodPost, "/login", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		c := echo.New().NewContext(req, rec)

		return c, rec
	}(model.LoginRequest{
		Username: "",
		Password: "testPassword123",
	})

	service := auth_mocks.NewMockservice(mockController)
	handler := New(service)

	service.EXPECT().
		DoLogin(gomock.Any(), gomock.Eq(&model.LoginRequest{
			Username: "",
			Password: "testPassword123",
		})).
		Return(nil, error2.GErr{
			Status:  http.StatusUnauthorized,
			Message: "unauthorized",
		}).
		Times(1)

	err := handler.Login(context)
	assert.NotNil(t, err)
	assert.Equal(t, error2.GErr{
		Status:  http.StatusUnauthorized,
		Message: "unauthorized",
	}, err)
}

func TestHandlerShouldReturnUnAuthorizedWhenPasswordNotProvided(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, _ := func(request model.LoginRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(request)
		req := httptest.NewRequest(http.MethodPost, "/login", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		c := echo.New().NewContext(req, rec)

		return c, rec
	}(model.LoginRequest{
		Username: "testUser",
		Password: "",
	})

	service := auth_mocks.NewMockservice(mockController)
	handler := New(service)

	service.EXPECT().
		DoLogin(gomock.Any(), gomock.Eq(&model.LoginRequest{
			Username: "testUser",
			Password: "",
		})).
		Return(nil, error2.GErr{
			Status:  http.StatusUnauthorized,
			Message: "unauthorized",
		}).
		Times(1)

	err := handler.Login(context)
	assert.NotNil(t, err)
	assert.Equal(t, error2.GErr{
		Status:  http.StatusUnauthorized,
		Message: "unauthorized",
	}, err)
}
