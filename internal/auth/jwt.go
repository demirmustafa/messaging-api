package auth

import (
	"crypto/rsa"
	"fmt"
	"time"

	"github.com/labstack/gommon/log"

	"com.gitlab/demirmustafa/messaging-api/internal/config"
	"github.com/golang-jwt/jwt"

	publicjwt "github.com/golang-jwt/jwt/v4"
	"github.com/lestrrat-go/jwx/jwk"
)

type Issuer struct {
	publicKey    *rsa.PublicKey
	privateKey   *rsa.PrivateKey
	issuer       string
	publicKeyJWK jwk.Key
	ttl          time.Duration
}

func NewIssuer(conf *config.JWTConf) (*Issuer, error) {
	privateKey, err := publicjwt.ParseRSAPrivateKeyFromPEM([]byte(conf.PrivateKey))
	if err != nil {
		return nil, err
	}

	publicKey, err := publicjwt.ParseRSAPublicKeyFromPEM([]byte(conf.PublicKey))
	if err != nil {
		return nil, err
	}

	publicKeyJWK, err := jwk.New(publicKey)
	if err != nil {
		return nil, err
	}

	return &Issuer{
		privateKey:   privateKey,
		publicKey:    publicKey,
		issuer:       conf.Issuer,
		publicKeyJWK: publicKeyJWK,
		ttl:          time.Duration(conf.TTL),
	}, nil
}

func (ir *Issuer) Generate(payload map[string]interface{}) (string, error) {
	pl := publicjwt.MapClaims(payload)
	pl["exp"] = time.Now().Add(ir.ttl * time.Second).Unix()
	pl["iss"] = ir.issuer
	pl["sub"] = ir.issuer
	token := publicjwt.NewWithClaims(
		publicjwt.SigningMethodRS256,
		pl,
	)

	tokenString, err := token.SignedString(ir.privateKey)
	if err != nil {
		log.Error(err)
		return "", err
	}
	return tokenString, nil
}

func (ir *Issuer) PublicKeyJWKs() jwk.Key { // may not to be needed
	return ir.publicKeyJWK
}

func (ir *Issuer) Parse(tokenString string) (map[string]interface{}, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return ir.publicKey, nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}
	return nil, err
}

func (ir *Issuer) IsTokenExpired(claims map[string]interface{}) bool {
	exp := int64(claims["exp"].(float64))
	expirationTime := time.Unix(exp, 0)
	now := time.Now()
	return expirationTime.Before(now)
}
