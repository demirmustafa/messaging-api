package auth

import (
	"net/http"
	"strings"

	error2 "com.gitlab/demirmustafa/messaging-api/pkg/error"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

var unAuthorizedErr = error2.GErr{
	Status:  http.StatusUnauthorized,
	Message: "unauthorized",
}

type RequestInterceptor interface {
	InterceptRequest(handlerFunc echo.HandlerFunc) echo.HandlerFunc
}

type PreAuthMW struct {
	Issuer issuer
}

func (ri *PreAuthMW) InterceptRequest(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token, err := extractAccessTokenFromHeader(c)
		if err != nil {
			return err
		}

		claims, err := ri.Issuer.Parse(token)
		if err != nil {
			log.Error(err)
			return unAuthorizedErr
		}
		expired := ri.Issuer.IsTokenExpired(claims)
		if expired {
			log.Info("token expired")
			return unAuthorizedErr
		}
		username := claims["username"]
		if username == nil {
			return unAuthorizedErr
		}
		c.Set("username", username.(string))
		err = next(c)
		return err
	}
}

func extractAccessTokenFromHeader(c echo.Context) (string, error) {
	bearerToken := c.Request().Header.Get(echo.HeaderAuthorization)
	if bearerToken == "" || !strings.Contains(bearerToken, "Bearer ") {
		return "", unAuthorizedErr
	}

	accessToken := strings.Replace(bearerToken, "Bearer ", "", 1)
	if accessToken == "" {
		return "", unAuthorizedErr
	}
	return accessToken, nil
}
