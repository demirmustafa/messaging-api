package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SendMessageRequest struct {
	Receiver string `json:"receiver"`
	Message  string `json:"message"`
}

type Message struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	Sender       string             `bson:"sender"`
	Receiver     string             `bson:"receiver"`
	Message      string             `bson:"message"`
	CreationTime int64              `bson:"creation_time"`
}

type MessageResource struct {
	ID           string `json:"id"`
	Sender       string `json:"sender"`
	Receiver     string `json:"receiver"`
	Message      string `json:"message"`
	CreationTime string `json:"time"`
}
