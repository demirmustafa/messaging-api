package message

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	message_mocks "com.gitlab/demirmustafa/messaging-api/internal/message/mocks"
	"com.gitlab/demirmustafa/messaging-api/internal/message/model"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestHandlerShouldSendMessage(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, recorder := func(sendMessageRequest model.SendMessageRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(sendMessageRequest)
		req := httptest.NewRequest(http.MethodPost, "/messages", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		c := echo.New().NewContext(req, rec)
		c.Set("username", "testSender")

		return c, rec
	}(model.SendMessageRequest{
		Receiver: "testReceiver",
		Message:  "hello",
	})

	service := message_mocks.NewMockservice(mockController)
	service.EXPECT().
		SendMessage(gomock.Any(), gomock.Eq("testSender"), gomock.Eq(&model.SendMessageRequest{Receiver: "testReceiver", Message: "hello"})).
		Return(nil).
		Times(1)

	handler := New(service, nil)
	err := handler.SendMessage(context)

	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, recorder.Code)
}

func TestHandlerShouldReturnErrorResponseWhenTryingToSendMessageToNotExistentUser(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, _ := func(sendMessageRequest model.SendMessageRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(sendMessageRequest)
		req := httptest.NewRequest(http.MethodPost, "/messages", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		c := echo.New().NewContext(req, rec)
		c.Set("username", "testSender")

		return c, rec
	}(model.SendMessageRequest{
		Receiver: "unExistReceiver",
		Message:  "hello",
	})

	service := message_mocks.NewMockservice(mockController)
	handler := New(service, nil)

	service.EXPECT().
		SendMessage(gomock.Any(), gomock.Eq("testSender"), gomock.Eq(&model.SendMessageRequest{Receiver: "unExistReceiver", Message: "hello"})).
		Return(errors.New("un exist receiver")).
		Times(1)

	err := handler.SendMessage(context)

	assert.NotNil(t, err)
	assert.Equal(t, errors.New("un exist receiver"), err)
}

func TestHandlerShouldReturnMessages(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, recorder := func() (echo.Context, *httptest.ResponseRecorder) {
		req := httptest.NewRequest(http.MethodGet, "/messages/participant/:username", http.NoBody)
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		c := echo.New().NewContext(req, rec)
		c.Set("username", "testSender")
		c.SetParamNames("username")
		c.SetParamValues("testReceiver")

		return c, rec
	}()

	expected := []*model.MessageResource{
		{
			Sender:       "testReceiver",
			Receiver:     "testSender",
			Message:      "hi :)",
			CreationTime: "2021-12-19T12:30:00Z",
		},
		{
			Sender:       "testSender",
			Receiver:     "testReceiver",
			Message:      "hi",
			CreationTime: "2021-12-19T12:20:00Z",
		},
	}

	service := message_mocks.NewMockservice(mockController)
	service.EXPECT().
		GetMessages(gomock.Any(), gomock.Eq("testSender"), gomock.Eq("testReceiver")).
		Return(expected, nil).Times(1)
	handler := New(service, nil)

	err := handler.GetMessages(context)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, recorder.Code)
	var response []*model.MessageResource
	err = json.Unmarshal(recorder.Body.Bytes(), &response)
	assert.Nil(t, err)
	assert.Equal(t, expected, response)
}
