package message

import (
	"context"
	"net/http"
	time2 "time"

	"com.gitlab/demirmustafa/messaging-api/internal/message/model"
	errs "com.gitlab/demirmustafa/messaging-api/pkg/error"
	"com.gitlab/demirmustafa/messaging-api/pkg/time"
)

type Service struct {
	timer      time.Timer
	repository repository
}

type repository interface {
	SaveMessage(ctx context.Context, message *model.Message) error
	GetMessagesBetweenParticipantsOrderByCreationTimeDesc(ctx context.Context, participant1, participant2 string) []*model.Message
}

func NewService(r repository, t time.Timer) *Service {
	return &Service{repository: r, timer: t}
}

func (s *Service) SendMessage(ctx context.Context, from string, request *model.SendMessageRequest) error {
	if request == nil || request.Receiver == "" {
		return errs.GErr{
			Status:  http.StatusBadRequest,
			Message: "receiver must be provided",
		}
	}

	// todo check if receiver exists

	now := s.timer.GetNow()
	m := &model.Message{
		Sender:       from,
		Receiver:     request.Receiver,
		Message:      request.Message,
		CreationTime: now.Unix(),
	}
	return s.repository.SaveMessage(ctx, m)
}

func (s *Service) GetMessages(ctx context.Context, participant1, participant2 string) ([]*model.MessageResource, error) {
	if participant1 == "" || participant2 == "" {
		return nil, errs.GErr{
			Status:  http.StatusBadRequest,
			Message: "participants must be provided",
		}
	}
	messages := s.repository.GetMessagesBetweenParticipantsOrderByCreationTimeDesc(ctx, participant1, participant2)
	response := make([]*model.MessageResource, 0)
	for _, m := range messages {
		resource := convertMessageToResource(m)
		response = append(response, resource)
	}
	return response, nil
}

func convertMessageToResource(m *model.Message) *model.MessageResource {
	return &model.MessageResource{
		ID:           m.ID.Hex(),
		Sender:       m.Sender,
		Receiver:     m.Receiver,
		Message:      m.Message,
		CreationTime: time2.Unix(m.CreationTime, 0).String(),
	}
}
