package message

import (
	"context"
	"net/http"

	"com.gitlab/demirmustafa/messaging-api/internal/auth"
	"com.gitlab/demirmustafa/messaging-api/internal/message/model"
	"github.com/labstack/echo/v4"
)

type Handler struct {
	service     service
	interceptor auth.RequestInterceptor
}

type service interface {
	SendMessage(ctx context.Context, from string, request *model.SendMessageRequest) error
	GetMessages(ctx context.Context, participant1, participant2 string) ([]*model.MessageResource, error)
}

func New(s service, i auth.RequestInterceptor) *Handler {
	return &Handler{service: s, interceptor: i}
}

func (h Handler) RegisterRoutes(app *echo.Echo) {
	app.POST("/messages", h.SendMessage, h.interceptor.InterceptRequest)
	app.GET("/messages/participant/:username", h.GetMessages, h.interceptor.InterceptRequest)
}

func (h Handler) SendMessage(ctx echo.Context) error {
	c := ctx.Request().Context()
	from := ctx.Get("username").(string)
	request := &model.SendMessageRequest{}
	err := ctx.Bind(request)
	if err != nil {
		return err
	}
	return h.service.SendMessage(c, from, request)
}

func (h Handler) GetMessages(ctx echo.Context) error {
	c := ctx.Request().Context()
	p1 := ctx.Get("username").(string)
	p2 := ctx.Param("username")
	messages, err := h.service.GetMessages(c, p1, p2)
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, messages)
}
