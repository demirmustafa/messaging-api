package message

import (
	"context"
	"net/http"
	"testing"
	time2 "time"

	message_mocks "com.gitlab/demirmustafa/messaging-api/internal/message/mocks"
	"com.gitlab/demirmustafa/messaging-api/internal/message/model"
	errs "com.gitlab/demirmustafa/messaging-api/pkg/error"
	"com.gitlab/demirmustafa/messaging-api/pkg/time"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestServiceShouldSaveMessage(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	timer := time.NewMockTimer(mockController)
	now := time2.Now()
	timer.EXPECT().
		GetNow().
		Return(&now).
		Times(1)
	repository := message_mocks.NewMockrepository(mockController)
	repository.EXPECT().
		SaveMessage(gomock.Any(), gomock.Eq(&model.Message{
			Sender:       "testSender",
			Receiver:     "testReceiver",
			Message:      "test message",
			CreationTime: now.Unix(),
		}))

	service := NewService(repository, timer)
	err := service.SendMessage(context.Background(), "testSender", &model.SendMessageRequest{
		Receiver: "testReceiver",
		Message:  "test message",
	})

	assert.Nil(t, err)
}

func TestServiceShouldReturnErrorResponseWhenMessageRequestIsInvalid(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	timer := time.NewMockTimer(mockController)
	repository := message_mocks.NewMockrepository(mockController)

	service := NewService(repository, timer)
	err := service.SendMessage(context.Background(), "testSender", nil)

	assert.NotNil(t, err)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "receiver must be provided",
	}, err)
}

func TestServiceShouldReturnErrorResponseWhenReceiverNotSpecified(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	timer := time.NewMockTimer(mockController)
	repository := message_mocks.NewMockrepository(mockController)

	service := NewService(repository, timer)
	err := service.SendMessage(context.Background(), "testSender", &model.SendMessageRequest{Message: "test message"})

	assert.NotNil(t, err)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "receiver must be provided",
	}, err)
}

func TestServiceShouldReturnMessages(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	timer := time.NewMockTimer(mockController)
	repository := message_mocks.NewMockrepository(mockController)

	location := time2.Now().Location()
	t1, _ := time2.ParseInLocation("2006-01-02T15:04:05Z", "2021-12-19T12:20:00Z", location)
	t2, _ := time2.ParseInLocation("2006-01-02T15:04:05Z", "2021-12-19T12:30:00Z", location)
	messages := []*model.Message{
		{
			Sender:       "testReceiver",
			Receiver:     "testSender",
			Message:      "hi :)",
			CreationTime: t2.Unix(),
		},
		{
			Sender:       "testSender",
			Receiver:     "testReceiver",
			Message:      "hi",
			CreationTime: t1.Unix(),
		},
	}

	expected := []*model.MessageResource{
		{
			Sender:       "testReceiver",
			Receiver:     "testSender",
			Message:      "hi :)",
			CreationTime: t2.String(),
		},
		{
			Sender:       "testSender",
			Receiver:     "testReceiver",
			Message:      "hi",
			CreationTime: t1.String(),
		},
	}

	repository.EXPECT().
		GetMessagesBetweenParticipantsOrderByCreationTimeDesc(gomock.Any(), gomock.Eq("testSender"), gomock.Eq("testReceiver")).
		Return(messages).
		Times(1)

	service := NewService(repository, timer)
	actual, err := service.GetMessages(context.Background(), "testSender", "testReceiver")

	assert.Nil(t, err)
	assert.Equal(t, len(expected), len(actual))
	assert.Equal(t, expected[0].Sender, actual[0].Sender)
	assert.Equal(t, expected[0].Receiver, actual[0].Receiver)
	assert.Equal(t, expected[0].Message, actual[0].Message)
	assert.Equal(t, expected[0].CreationTime, actual[0].CreationTime)
}
