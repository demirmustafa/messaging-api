package store

import (
	"context"

	"github.com/labstack/gommon/log"

	"go.mongodb.org/mongo-driver/bson"

	"com.gitlab/demirmustafa/messaging-api/internal/user/model"
	"go.mongodb.org/mongo-driver/mongo"
)

type Repository struct {
	collection *mongo.Collection
}

func NewUserRepository(c *mongo.Collection) *Repository {
	return &Repository{collection: c}
}

func (r Repository) CreateUser(ctx context.Context, user *model.User) error {
	_, err := r.collection.InsertOne(ctx, user)
	return err
}

func (r Repository) GetUser(ctx context.Context, username string) (*model.User, error) {
	one := r.collection.FindOne(ctx, bson.M{"username": username})
	if one.Err() != nil {
		log.Error(one.Err())
		return nil, nil
	}
	u := &model.User{}
	err := one.Decode(u)

	if err != nil {
		log.Error(err)
	}
	return u, nil
}
