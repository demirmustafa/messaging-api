package store

import (
	"context"
	"fmt"
	"testing"
	"time"

	"com.gitlab/demirmustafa/messaging-api/internal/config"
	"github.com/docker/go-connections/nat"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

type DBSuite struct {
	suite.Suite
	Container  testcontainers.Container
	MongoStore *MongoStore
}

func TestMongo(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, new(DBSuite))
}

func (s *DBSuite) SetupSuite() {
	conf := &config.MongoConfig{
		Host:     "localhost",
		Port:     "27017",
		Database: "testdb",
		Username: "testUser",
		Password: "testPassword123",
	}

	mongoStore := New(conf)
	container, err := s.startMongoDBContainer(conf)
	assert.Nil(s.T(), err)

	s.Container = container
	s.MongoStore = mongoStore
}

func (s *DBSuite) TearDownSuite() {
	if s.Container != nil {
		_ = s.Container.Terminate(context.Background())
	}
}

func (s *DBSuite) startMongoDBContainer(c *config.MongoConfig) (testcontainers.Container, error) {
	port, err := nat.NewPort("tcp", c.Port)
	assert.Nil(s.T(), err)
	container, err := testcontainers.GenericContainer(context.Background(), testcontainers.GenericContainerRequest{
		ContainerRequest: testcontainers.ContainerRequest{
			Image:        "mongo:5.0",
			ExposedPorts: []string{fmt.Sprintf("%s:27017/tcp", c.Port)},
			WaitingFor:   wait.ForListeningPort(port).WithStartupTimeout(time.Second * 30),
			Env: map[string]string{
				"MONGO_INITDB_ROOT_USERNAME": c.Username,
				"MONGO_INITDB_ROOT_PASSWORD": c.Password,
			},
			Name:       "mongo-test",
			SkipReaper: false,
		},
		Started: true,
	})
	return container, err
}
