package store

import (
	"context"

	"com.gitlab/demirmustafa/messaging-api/internal/message/model"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MessageRepository struct {
	collection *mongo.Collection
}

func NewMessageRepository(c *mongo.Collection) *MessageRepository {
	return &MessageRepository{collection: c}
}

func (m MessageRepository) SaveMessage(ctx context.Context, message *model.Message) error {
	_, err := m.collection.InsertOne(ctx, message)
	return err
}

func (m MessageRepository) GetMessagesBetweenParticipantsOrderByCreationTimeDesc(ctx context.Context, participant1,
	participant2 string) []*model.Message {
	opts := options.Find().SetSort(bson.D{{Key: "creation_time", Value: -1}})
	findQuery := bson.M{}
	var orQuery []bson.M
	orQuery = append(orQuery, bson.M{"sender": participant1, "receiver": participant2},
		bson.M{"sender": participant2, "receiver": participant1})

	findQuery["$or"] = orQuery

	result := make([]*model.Message, 0)
	found, err := m.collection.Find(ctx, findQuery, opts)
	if err != nil {
		log.Error(err)
	}

	err = found.All(ctx, &result)
	if err != nil {
		return nil
	}
	return result
}
