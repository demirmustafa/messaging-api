package store

import (
	"context"

	"com.gitlab/demirmustafa/messaging-api/internal/user/model"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

func (s *DBSuite) TestRepositoryShouldSaveDocumentToDB() {
	collection := s.MongoStore.Collection("users")
	repository := NewUserRepository(collection)

	userToCreate := &model.User{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "testPassword123",
	}
	err := repository.CreateUser(context.Background(), userToCreate)

	assert.Nil(s.T(), err)
	found := collection.FindOne(context.Background(), bson.M{"username": "testUser"})
	u := &model.User{}
	err = found.Decode(u)
	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), u)
	assert.NotNil(s.T(), u.ID)
	assert.Equal(s.T(), userToCreate.Email, u.Email)
	assert.Equal(s.T(), userToCreate.Username, u.Username)
	assert.Equal(s.T(), userToCreate.Password, u.Password)
}

func (s *DBSuite) TestRepositoryShouldGetUserByUsername() {
	collection := s.MongoStore.Collection("users")
	repository := NewUserRepository(collection)

	ctx := context.Background()
	_, err := collection.InsertOne(ctx, bson.D{
		bson.E{Key: "email", Value: "user@test.com"},
		bson.E{Key: "username", Value: "testUser"},
		bson.E{Key: "password", Value: "testPassword123"},
	})

	assert.Nil(s.T(), err)
	user, err := repository.GetUser(ctx, "testUser")
	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), user)
	assert.Equal(s.T(), "user@test.com", user.Email)
	assert.Equal(s.T(), "testUser", user.Username)
	assert.Equal(s.T(), "testPassword123", user.Password)
}
