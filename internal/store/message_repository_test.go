package store

import (
	"context"
	"time"

	"com.gitlab/demirmustafa/messaging-api/internal/message/model"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

func (s *DBSuite) TestRepositoryShouldSaveMessageToDB() {
	collection := s.MongoStore.Collection("messages")
	repository := NewMessageRepository(collection)

	location := time.Now().Location()
	t, _ := time.ParseInLocation("2006-01-02T15:04:05Z", "2021-12-19T12:20:00Z", location)
	m := &model.Message{
		Sender:       "testSender1",
		Receiver:     "testReceiver1",
		Message:      "test message",
		CreationTime: t.Unix(),
	}
	err := repository.SaveMessage(context.Background(), m)

	assert.Nil(s.T(), err)
	found := collection.FindOne(context.Background(), bson.M{"sender": "testSender1"})
	saved := &model.Message{}
	err = found.Decode(saved)
	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), saved)
	assert.NotNil(s.T(), saved.ID)
	assert.Equal(s.T(), m.Sender, saved.Sender)
	assert.Equal(s.T(), m.Receiver, saved.Receiver)
	assert.Equal(s.T(), m.Message, saved.Message)
	assert.Equal(s.T(), t.Unix(), saved.CreationTime)
}

func (s *DBSuite) TestRepositoryShouldGetAllMessagesBetweenSenderAndReceiver() {
	ctx := context.Background()
	collection := s.MongoStore.Collection("messages")
	repository := NewMessageRepository(collection)
	location := time.Now().Location()
	t1, _ := time.ParseInLocation("2006-01-02T15:04:05Z", "2021-12-19T12:20:00Z", location)
	t2, _ := time.ParseInLocation("2006-01-02T15:04:05Z", "2021-12-19T12:30:00Z", location)
	m1 := &model.Message{
		Sender:       "testSender",
		Receiver:     "testReceiver",
		Message:      "hi",
		CreationTime: t1.Unix(),
	}

	m2 := &model.Message{
		Sender:       "testReceiver",
		Receiver:     "testSender",
		Message:      "hi :)",
		CreationTime: t2.Unix(),
	}

	_, _ = collection.InsertOne(ctx, m1)
	_, _ = collection.InsertOne(ctx, m2)

	messages := repository.GetMessagesBetweenParticipantsOrderByCreationTimeDesc(ctx, "testSender", "testReceiver")
	assert.Equal(s.T(), 2, len(messages))

	assert.Equal(s.T(), "testReceiver", messages[0].Sender)
	assert.Equal(s.T(), "testSender", messages[0].Receiver)
	assert.Equal(s.T(), "hi :)", messages[0].Message)
	assert.Equal(s.T(), t2.Unix(), messages[0].CreationTime)

	assert.Equal(s.T(), "testSender", messages[1].Sender)
	assert.Equal(s.T(), "testReceiver", messages[1].Receiver)
	assert.Equal(s.T(), "hi", messages[1].Message)
	assert.Equal(s.T(), t1.Unix(), messages[1].CreationTime)
}
