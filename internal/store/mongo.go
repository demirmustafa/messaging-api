package store

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/labstack/gommon/log"

	"com.gitlab/demirmustafa/messaging-api/internal/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoStore struct {
	database *mongo.Database
}

func New(c *config.MongoConfig) *MongoStore {
	var doOnce sync.Once
	var client *mongo.Client
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	doOnce.Do(func() {
		opts := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s", c.Host, c.Port))
		credentials := options.Credential{
			Username: c.Username,
			Password: c.Password,
		}
		opts = opts.SetAuth(credentials)

		c, err := mongo.Connect(ctx, opts)
		if err != nil {
			log.Fatal(err)
		}
		client = c
	})

	database := client.Database(c.Database)
	return &MongoStore{database: database}
}

func (m *MongoStore) Collection(collection string) *mongo.Collection {
	return m.database.Collection(collection)
}
