package user

import (
	"context"
	"errors"
	"net/http"

	errs "com.gitlab/demirmustafa/messaging-api/pkg/error"

	"com.gitlab/demirmustafa/messaging-api/internal/user/model"
)

type Service struct {
	repository repository
}

type repository interface {
	CreateUser(ctx context.Context, user *model.User) error
	GetUser(ctx context.Context, username string) (*model.User, error)
}

func NewService(r repository) *Service {
	return &Service{repository: r}
}

func (s *Service) CreateUser(ctx context.Context, request *model.CreateUserRequest) error {
	if err := validateRequest(request); err != nil {
		return err
	}

	user, err := s.repository.GetUser(ctx, request.Username)
	if user != nil {
		return errs.GErr{
			Status:  409,
			Message: "An user already exist with same username",
		}
	}

	if err != nil {
		return err
	}

	u := &model.User{
		Email:    request.Email,
		Username: request.Username,
		Password: request.Password,
	}
	return s.repository.CreateUser(ctx, u)
}

func (s *Service) GetUser(ctx context.Context, username string) (*model.User, error) {
	if username == "" {
		return nil, errors.New("username can not be empty")
	}
	return s.repository.GetUser(ctx, username)
}

func validateRequest(request *model.CreateUserRequest) error {
	if request.Email == "" {
		return errs.GErr{
			Status:  http.StatusBadRequest,
			Message: "email must be provided",
		}
	}

	if request.Username == "" {
		return errs.GErr{
			Status:  http.StatusBadRequest,
			Message: "username must be provided",
		}
	}

	if request.Password == "" {
		return errs.GErr{
			Status:  http.StatusBadRequest,
			Message: "password must be provided",
		}
	}
	return nil
}
