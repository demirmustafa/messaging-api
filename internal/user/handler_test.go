package user

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	errs "com.gitlab/demirmustafa/messaging-api/pkg/error"

	user_mocks "com.gitlab/demirmustafa/messaging-api/internal/user/mocks"
	"com.gitlab/demirmustafa/messaging-api/internal/user/model"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestHandlerShouldRegisterUser(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, recorder := func(createAccountRequest model.CreateUserRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(createAccountRequest)
		req := httptest.NewRequest(http.MethodPost, "/register", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		c := echo.New().NewContext(req, rec)

		return c, rec
	}(model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "testPass123",
	})

	service := user_mocks.NewMockUserService(mockController)
	handler := New(service)

	service.EXPECT().CreateUser(gomock.Any(), gomock.Eq(&model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "testPass123",
	})).Return(nil).Times(1)

	err := handler.Register(context)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusCreated, recorder.Code)
}

func TestHandlerShouldRespondWithBadRequestStatusWhenAccountCreationRequestDoesNotHaveEmail(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, _ := func(createAccountRequest model.CreateUserRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(createAccountRequest)
		req := httptest.NewRequest(http.MethodPost, "/register", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		app := echo.New()
		c := app.NewContext(req, rec)

		return c, rec
	}(model.CreateUserRequest{
		Email:    "",
		Username: "testUser",
		Password: "testPass123",
	})

	service := user_mocks.NewMockUserService(mockController)
	handler := New(service)

	service.EXPECT().CreateUser(gomock.Any(), gomock.Eq(&model.CreateUserRequest{
		Email:    "",
		Username: "testUser",
		Password: "testPass123",
	})).Return(errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "email must be provided",
	}).Times(1)

	err := handler.Register(context)
	assert.NotNil(t, err)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "email must be provided",
	}, err)
}

func TestHandlerShouldRespondWithBadRequestStatusWhenAccountCreationRequestDoesNotHaveUsername(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, _ := func(createAccountRequest model.CreateUserRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(createAccountRequest)
		req := httptest.NewRequest(http.MethodPost, "/register", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		app := echo.New()
		c := app.NewContext(req, rec)

		return c, rec
	}(model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "",
		Password: "testPass123",
	})

	service := user_mocks.NewMockUserService(mockController)
	handler := New(service)

	service.EXPECT().CreateUser(gomock.Any(), gomock.Eq(&model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "",
		Password: "testPass123",
	})).Return(errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "username must be provided",
	}).Times(1)

	err := handler.Register(context)
	assert.NotNil(t, err)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "username must be provided",
	}, err)
}

func TestHandlerShouldRespondWithBadRequestStatusWhenAccountCreationRequestDoesNotHavePassword(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	context, _ := func(createAccountRequest model.CreateUserRequest) (echo.Context, *httptest.ResponseRecorder) {
		body, _ := json.Marshal(createAccountRequest)
		req := httptest.NewRequest(http.MethodPost, "/register", bytes.NewReader(body))
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")

		rec := httptest.NewRecorder()
		app := echo.New()
		c := app.NewContext(req, rec)

		return c, rec
	}(model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "",
	})

	service := user_mocks.NewMockUserService(mockController)
	handler := New(service)

	service.EXPECT().CreateUser(gomock.Any(), gomock.Eq(&model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "",
	})).Return(errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "password must be provided",
	}).Times(1)

	err := handler.Register(context)
	assert.NotNil(t, err)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "password must be provided",
	}, err)
}
