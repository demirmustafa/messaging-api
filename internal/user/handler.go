package user

import (
	"context"
	"net/http"

	"com.gitlab/demirmustafa/messaging-api/internal/user/model"
	"github.com/labstack/echo/v4"
)

type Handler struct {
	service UserService
}

type UserService interface { // nolint:revive
	CreateUser(ctx context.Context, request *model.CreateUserRequest) error
	GetUser(ctx context.Context, username string) (*model.User, error)
}

func New(service UserService) Handler {
	return Handler{service: service}
}

func (h Handler) RegisterRoutes(app *echo.Echo) {
	app.POST("/register", h.Register)
}

func (h Handler) Register(ctx echo.Context) error {
	request := &model.CreateUserRequest{}
	err := ctx.Bind(request)
	if err != nil {
		return err
	}
	err = h.service.CreateUser(ctx.Request().Context(), request)
	if err != nil {
		return err
	}
	return ctx.NoContent(http.StatusCreated)
}
