package user

import (
	"context"
	"errors"
	"net/http"
	"testing"

	errs "com.gitlab/demirmustafa/messaging-api/pkg/error"

	user_mocks "com.gitlab/demirmustafa/messaging-api/internal/user/mocks"
	"com.gitlab/demirmustafa/messaging-api/internal/user/model"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestServiceShouldRegisterUser(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	repository := user_mocks.NewMockrepository(mockController)
	service := NewService(repository)

	u := &model.User{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "testPassword123",
	}

	repository.EXPECT().
		GetUser(gomock.Any(), gomock.Eq("testUser")).
		Return(nil, nil).
		Times(1)

	repository.
		EXPECT().
		CreateUser(gomock.Any(), gomock.Eq(u)).
		Return(nil).
		Times(1)

	err := service.CreateUser(context.Background(), &model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "testPassword123",
	})
	assert.Nil(t, err)
}

func TestServiceShouldReturnValidationErrorWhenEmailNotProvided(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	repository := user_mocks.NewMockrepository(mockController)
	service := NewService(repository)

	repository.EXPECT().
		CreateUser(gomock.Any(), gomock.Any()).
		Times(0)

	err := service.CreateUser(context.Background(), &model.CreateUserRequest{
		Email:    "",
		Username: "testUser",
		Password: "testPassword123",
	})
	assert.NotNil(t, err)
	var gErr errs.GErr
	isGErr := errors.As(err, &gErr)
	assert.True(t, isGErr)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "email must be provided",
	}, gErr)
}

func TestServiceShouldReturnValidationErrorWhenUsernameNotProvided(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	repository := user_mocks.NewMockrepository(mockController)
	service := NewService(repository)

	repository.EXPECT().
		CreateUser(gomock.Any(), gomock.Any()).
		Times(0)

	err := service.CreateUser(context.Background(), &model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "",
		Password: "testPassword123",
	})
	assert.NotNil(t, err)
	var gErr errs.GErr
	isGErr := errors.As(err, &gErr)
	assert.True(t, isGErr)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "username must be provided",
	}, gErr)
}

func TestServiceShouldReturnValidationErrorWhenPasswordNotProvided(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	repository := user_mocks.NewMockrepository(mockController)
	service := NewService(repository)

	repository.EXPECT().
		CreateUser(gomock.Any(), gomock.Any()).
		Times(0)

	err := service.CreateUser(context.Background(), &model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "",
	})
	assert.NotNil(t, err)
	var gErr errs.GErr
	isGErr := errors.As(err, &gErr)
	assert.True(t, isGErr)
	assert.Equal(t, errs.GErr{
		Status:  http.StatusBadRequest,
		Message: "password must be provided",
	}, gErr)
}

func TestServiceShouldReturnErrorWhenUserAlreadyExists(t *testing.T) {
	mockController := gomock.NewController(t)
	defer mockController.Finish()

	repository := user_mocks.NewMockrepository(mockController)
	service := NewService(repository)

	repository.EXPECT().
		GetUser(gomock.Any(), gomock.Eq("testUser")).
		Return(&model.User{}, errors.New("already exist")).
		Times(1)

	repository.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Times(0)

	err := service.CreateUser(context.Background(), &model.CreateUserRequest{
		Email:    "user@test.com",
		Username: "testUser",
		Password: "testPassword123",
	})

	assert.NotNil(t, err)
	var gErr errs.GErr
	isGErr := errors.As(err, &gErr)
	assert.True(t, isGErr)
	assert.Equal(t, errs.GErr{
		Status:  409,
		Message: "An user already exist with same username",
	}, gErr)
}
