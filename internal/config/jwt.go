package config

type JWTConf struct {
	Issuer     string
	PrivateKey string
	PublicKey  string
	TTL        int
}
