package config

import (
	"encoding/base64"
	"fmt"
	"os"
	"strconv"
)

type AppConfig struct {
	JWT JWTConf
	DB  MongoConfig
}

func ReadConf() (*AppConfig, error) {
	conf := &AppConfig{}
	mongo, err := readDBConf()
	if err != nil {
		return nil, err
	}
	conf.DB = *mongo

	jwt, err := readJWTConf()
	if err != nil {
		return nil, err
	}
	conf.JWT = *jwt

	println(conf)
	return conf, nil
}

func readJWTConf() (*JWTConf, error) {
	privateKeyBase64 := os.Getenv("JWT_PRIVATE_KEY")
	if privateKeyBase64 == "" {
		return nil, fmt.Errorf("JWT_PRIVATE_KEY env must be provided")
	}

	publicKeyBase64 := os.Getenv("JWT_PUBLIC_KEY")
	if publicKeyBase64 == "" {
		return nil, fmt.Errorf("JWT_PUBLIC_KEY env must be provided")
	}

	ttl := os.Getenv("JWT_TTL")
	if ttl == "" {
		return nil, fmt.Errorf("JWT_TTL env must be provided")
	}

	issuer := os.Getenv("JWT_ISSUER")
	if issuer == "" {
		return nil, fmt.Errorf("JWT_ISSUER env must be provided")
	}

	ttlAsInt, err := strconv.Atoi(ttl)
	if err != nil {
		return nil, err
	}

	privateKey, err := base64.StdEncoding.DecodeString(privateKeyBase64)
	if err != nil {
		return nil, err
	}

	publicKey, err := base64.StdEncoding.DecodeString(publicKeyBase64)
	if err != nil {
		return nil, err
	}
	return &JWTConf{
		Issuer:     issuer,
		PrivateKey: string(privateKey),
		PublicKey:  string(publicKey),
		TTL:        ttlAsInt,
	}, nil
}

func readDBConf() (*MongoConfig, error) {
	host := os.Getenv("DB_HOST")
	if host == "" {
		return nil, fmt.Errorf("DB_HOST env must be provided")
	}

	port := os.Getenv("DB_PORT")
	if port == "" {
		return nil, fmt.Errorf("DB_PORT env must be provided")
	}

	username := os.Getenv("DB_USERNAME")
	if username == "" {
		return nil, fmt.Errorf("DB_USERNAME env must be provided")
	}

	password := os.Getenv("DB_PASSWORD")
	if password == "" {
		return nil, fmt.Errorf("DB_PASSWORD env must be provided")
	}

	name := os.Getenv("DB_NAME")
	if name == "" {
		return nil, fmt.Errorf("DB_NAME env must be provided")
	}

	return &MongoConfig{
		Host:     host,
		Port:     port,
		Database: name,
		Username: username,
		Password: password,
	}, nil
}
