package config

type MongoConfig struct {
	Host     string
	Port     string
	Database string
	Username string
	Password string
}
