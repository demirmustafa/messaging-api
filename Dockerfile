FROM golang:alpine as build
WORKDIR /app
COPY . .
RUN go build -o messaging-api ./cmd


FROM alpine:latest
EXPOSE 8080
COPY --from=build /app/messaging-api .
CMD [ "./messaging-api" ]