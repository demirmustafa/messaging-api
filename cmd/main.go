package main

import (
	"com.gitlab/demirmustafa/messaging-api/internal/auth"
	"com.gitlab/demirmustafa/messaging-api/internal/config"
	"com.gitlab/demirmustafa/messaging-api/internal/message"
	"com.gitlab/demirmustafa/messaging-api/internal/store"
	"com.gitlab/demirmustafa/messaging-api/internal/user"
	"com.gitlab/demirmustafa/messaging-api/pkg/server"
	"com.gitlab/demirmustafa/messaging-api/pkg/time"
	"github.com/labstack/gommon/log"
)

func main() {
	conf, err := config.ReadConf()
	if err != nil {
		log.Fatal(err)
	}

	mongo := store.New(&conf.DB)
	userRepository := store.NewUserRepository(mongo.Collection("users"))
	userService := user.NewService(userRepository)
	userHandler := user.New(userService)

	issuer, err := auth.NewIssuer(&conf.JWT)
	if err != nil {
		log.Fatal(err)
	}
	authService := auth.NewService(userService, issuer)
	authHandler := auth.New(authService)

	timer := time.NewTimer()
	messageRepository := store.NewMessageRepository(mongo.Collection("messages"))
	messageService := message.NewService(messageRepository, timer)
	preAuthMW := &auth.PreAuthMW{Issuer: issuer}
	messageHandler := message.New(messageService, preAuthMW)

	s := server.New(":8080", []server.Handler{userHandler, authHandler, messageHandler})
	s.Run()
}
