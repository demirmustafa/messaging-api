module com.gitlab/demirmustafa/messaging-api

go 1.16

require (
	github.com/docker/go-connections v0.4.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-jwt/jwt/v4 v4.2.0
	github.com/golang/mock v1.6.0
	github.com/labstack/echo/v4 v4.6.1
	github.com/labstack/gommon v0.3.0
	github.com/lestrrat-go/jwx v1.2.13
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	github.com/stretchr/testify v1.7.0
	github.com/testcontainers/testcontainers-go v0.12.0
	github.com/tylerb/graceful v1.2.15
	go.mongodb.org/mongo-driver v1.8.1
)
