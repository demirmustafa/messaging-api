# Messaging API

- [Postman Collection](https://www.getpostman.com/collections/3ac2ded292899fcccfa1)

## Requirements
- [Go:1.16](https://go.dev/doc/install)
- [Docker](https://www.docker.com/)

## Features
* Register
* Login
* Send Message to Known User
* Get Messages

## Tech Stack
* Go: 1.16
* Labstack Echo: v4
* MongoDB
* TestContainers

## Project Structure

- [Go Project Layout] (https://github.com/golang-standards/project-layout)
```
.        
├── .cd
|   ├── .gitlab-ci.yml        # CI/CD Pipeline conf file 
|   ├── .golangci.yml         # Conf file for linter
├── cmd                       
|   ├── main.go               # main function, app runner/starter
|   |
├── internal                  # business code
|   ├── auth                  # handler, service and unit tests for authentication (jwt)
|   ├── config                # config struct/models to map runtime environments from yml file
|   ├── message               # handler, service and unit tests for send/get message
|   ├── store                 # db connection, repositories and integration tests in here
|   ├── user                  # handler, service and unit tests for create user opertion
|   |
├── pkg                       # external, 3th party lib init etc.
|   ├── error                 # custom error models
|   ├── server                # echo server init
|   ├── time                  # timer interface for testing/mocking purpose
├── .gitignore
├── docker-compose.yml
├── Dockerfile
├── README.md
├── go.mod
```


### Run with Docker Compose
```
$ docker-compose up -d
```

### Terminate Docker Compose App
```
$ docker-compose down -v
```

## Build and Tests
### Building the project

```
$ go build -o messaging-api ./cmd
```

### Running tests

Run unit/integration tests
```
$ go test -v -count=1 ./...
```

To skip integration tests
```
$ go test -short -v -count=1 ./...
```
